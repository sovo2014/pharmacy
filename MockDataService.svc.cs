﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PharmacyProject
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MockDataService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MockDataService.svc or MockDataService.svc.cs at the Solution Explorer and start debugging.
    public class MockDataService : IMockDataService
    {
	    private const String CacheId = "E5221133-ED5F-4148-BFBD-DC4A609CB6DE";

	    private static void InitializeStorage()
		{
			var list = new List<Reaction>
                {
                    new Reaction
                        {
                            Cause = "Aspirin",
                            CauseType = "Drug",
                            Id = 1,
                            ReactionDescription = "Stomach Pain",
                            ReactionType = "Adverse",
                            ReportDate = DateTime.Today
                        },
                    new Reaction
                        {
                            Cause = "Mushrooms",
                            CauseType = "Food",
                            Id = 2,
                            ReactionDescription = "Hallucinations",
                            ReactionType = "Allergy",
                            ReportDate = DateTime.Today.AddDays(1)
                        }
                };

			var cacheItem = new CacheItem(CacheId, list);
			var expiration = DateTimeOffset.UtcNow.AddDays(7); 
			var policy = new CacheItemPolicy { AbsoluteExpiration = expiration }; 
			MemoryCache.Default.Add(cacheItem, policy);		
		}
        public List<Reaction> GetReactions()
        {
			if(MemoryCache.Default.Get(CacheId)==null)
				InitializeStorage();
	        return MemoryCache.Default.Get(CacheId) as List<Reaction>;
        }

	    public Reaction GetReaction(string id)
	    {
		    var reactions = GetReactions();
		    return reactions.FirstOrDefault(r => r.Id == int.Parse(id));
	    }

	    public string SaveReaction(string id, Reaction model)
	    {
			var reaction = GetReactions().First(r=>r.Id == int.Parse(id));
		    if (reaction == null) return "The record wasn't found!";
		    reaction.Cause = model.Cause;
		    reaction.CauseType = model.CauseType;
		    reaction.ReactionDescription = model.ReactionDescription;
		    reaction.ReactionType = model.ReactionType;
		    return String.Empty;
	    }

        public string AddReaction(Reaction reaction)
        {
            reaction.ReportDate = DateTime.Now;
            reaction.Id = GetReactions().Max(r => r.Id) + 1;
            GetReactions().Add(reaction);
            return string.Empty;
        }
    }
}
