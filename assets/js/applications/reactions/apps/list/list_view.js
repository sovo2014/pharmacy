﻿DrugReactionsManager.module("Reactions.List", function (List, DrugReactionsManager, Backbone, Marionette) {
	List.Reaction = Marionette.ItemView.extend({
		template: "#reactions-item-template",
		tagName: 'li',
		className: function() {
			var css = ['list-group-item'];
			css.push(this.model.get('ReactionType') === 'Allergy' ? 'list-group-item-warning' : 'list-group-item-danger');
			return css.join(' ');
		},
		onShow: function () {
			var self = this;
			this.$el.bind('click', function(ev) {
				ev.preventDefault();
				self.trigger('reaction:show', self.model);
			});
		},
		onBeforeClose: function () {
			this.$el.unbind('click');
		}
	});
	List.Reactions = Marionette.CompositeView.extend({
		tagName: 'div',
		className: 'panel panel-primary',
		template : '#reaction-list-template',
		itemView: List.Reaction,
		itemViewContainer: 'ul.list-group',
		events : {
			'click .js-btn-addNew' : 'addNewClicked'
		},
		addNewClicked : function(ev) {
			ev.preventDefault();
			this.trigger('reaction:add');
		}
	});
});