﻿DrugReactionsManager.module("Reactions.List", function (List, DrugReactionsManager, Backbone, Marionette) {
	List.Controller = {
		listReactions : function() {
			var fetchReactions = DrugReactionsManager.request("reaction:entities");
			$.when(fetchReactions).done(function(reactions) {
				var reactionsView = new List.Reactions({
					collection: reactions
				});
				reactionsView.on("itemview:reaction:show", function(childView, model) {
					DrugReactionsManager.trigger('reaction:show', model.id);
				});
				reactionsView.on('reaction:add', function() {
				    DrugReactionsManager.trigger('reaction:add');
				});
				DrugReactionsManager.mainRegion.show(reactionsView);
			});
		}
	};
});