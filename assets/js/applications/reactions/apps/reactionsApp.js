﻿DrugReactionsManager.module('Reactions', function (Reactions, DrugReactionsManager, Backbone, Marionette) {
	Reactions.Router = Marionette.AppRouter.extend({
		appRoutes: {
		    'reactions': 'listReactions',
		    'reactions/add' : 'showReaction',
			'reactions/:id' : 'showReaction'
		 }
	});
	DrugReactionsManager.on('reactions:list', function(){
		DrugReactionsManager.navigate('reactions');
		controller.listReactions();
	});

	DrugReactionsManager.on('reaction:show', function (id) {
		DrugReactionsManager.navigate('reactions/' + id);
		controller.showReaction(id);
	});
    
	DrugReactionsManager.on('reaction:add', function () {
	    DrugReactionsManager.navigate('reactions/add');
	    controller.showReaction();
	});

	var controller = {
		listReactions: function() {
			DrugReactionsManager.Reactions.List.Controller.listReactions();
		},
		showReaction : function(id) {
			DrugReactionsManager.Reactions.Show.Controller.showReaction(id);
		}
	};
	DrugReactionsManager.addInitializer(function() {
		var router = new Reactions.Router({
			controller: controller
		});
	});
});