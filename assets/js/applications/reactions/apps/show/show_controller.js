﻿DrugReactionsManager.module("Reactions.Show", function (Show, DrugReactionsManager, Backbone, Marionette) {
    function showReactionView(reaction) {
        var reactionView = new Show.ReactionView({
            model: reaction
        });
        reactionView.on('reactions:list', function () {
            DrugReactionsManager.trigger('reactions:list');
        });
        reactionView.on('reaction:save', function (data) {
            var saving = DrugReactionsManager.request("reaction:save", {model:reaction, attrs:data});
            $.when(saving).done(function(result) {
                if(result.error) {
                    reactionView.triggerMethod('save:data:invalid', result.error);
                }
                else {
                    DrugReactionsManager.trigger('reactions:list');
                }
            });
        });
        DrugReactionsManager.mainRegion.show(reactionView);
    }
    Show.Controller = {
        showReaction: function (id) {
            if(!id) {
                var newReaction = DrugReactionsManager.request("reaction:new");
                showReactionView(newReaction);
                return;
            }
			var fetchReaction = DrugReactionsManager.request("reaction:entity", id);
			$.when(fetchReaction).done(function (reaction) {
			    showReactionView(reaction);
			});
		}
	};
});