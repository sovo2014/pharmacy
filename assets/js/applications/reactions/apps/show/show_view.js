﻿DrugReactionsManager.module("Reactions.Show", function (Show, DrugReactionsManager, Backbone, Marionette) {
	Show.ReactionView = Marionette.ItemView.extend({
		template: '#reaction-form-template',
		tagName: 'form',
		attributes: {
			role : 'form'
		},
		events : {
			'click .js-btn-cancel': 'cancelClicked',
			'click .js-btn-submit' : 'submitClicked'
		},
		onShow: function () {
			var $el = this.$el;
			$('.btn-group[data-toggle-name]', $el).each(function () {
				var group = $(this);
				var name = group.attr('data-toggle-name');
				var hidden = $('input[name="' + name + '"]', $el);
				$('label', group).each(function () {
					var button = $(this).find('input:radio');
					$(this).on('click', function () {
						hidden.val(button.val());
					});
					if (button.val() === hidden.val()) {
						$(this).addClass('active');
					}
				});
			});
		},
		cancelClicked: function (ev) {
			ev.preventDefault();
			this.trigger('reactions:list');
		},
		submitClicked : function(ev) {
			ev.preventDefault();
			var data = Backbone.Syphon.serialize(this);
			this.trigger('reaction:save', data);
		},
		onSaveDataInvalid: function (errors) {
			var $el = this.$el;
			$('div.form-group').removeClass('has-error');
			_.each(errors, function(value, key) {
				$('input[name="' + key + '"]', $el).parent('.form-group').addClass('has-error');
			});
		}
	});
});