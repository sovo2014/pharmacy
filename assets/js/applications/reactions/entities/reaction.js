﻿DrugReactionsManager.module("Entities", function (Entities, DrugReactionsManager, Backbone) {

	Entities.Reaction = Backbone.Model.extend({
	    idAttribute: 'Id',
	    defaults: {
	        'Cause': '',
	        'ReactionDescription' : '',
	        'CauseType': 'Drug',
	        'ReactionType': 'Allergy'
	    },
	    url: function () {
	    	var baseUrl = '/MockDataService.svc/reactions';
		    if (!this.get('id'))
			    return baseUrl;
			return baseUrl + '/' + this.get('id');
		},
		validate: function(attrs, options) {
			var errors = {};
			if (!attrs.Cause) {
				errors.Cause = "Drug/Food/Environment name can't be empty";
			}
			if (!attrs.CauseType) {
				errors.CauseType = "Cause type can't be empty";
			}
			if (!attrs.ReactionType) {
				errors.ReactionType = "Reaction type can't be empty";
			}
			if (!attrs.ReactionDescription) {
				errors.ReactionDescription = "Reaction can't be empty";
			}
			if (! _.isEmpty(errors))
				return errors;
		},
		initialize: function () {
		    Backbone.Model.prototype.initialize.apply(this, arguments);
		    var error = this.validate(this.attributes);
		    if (error) {
		        this.trigger('invalid', this, error);
		    }
		}
	});

	Entities.ReactionsCollection = Backbone.Collection.extend({
	    model: Entities.Reaction,
	    url:'/MockDataService.svc/reactions'
	});
	DrugReactionsManager.reqres.setHandler("reaction:save", function (savingData) {
		var defer = $.Deferred();
		var savedModel = savingData.model, attrs = savingData.attrs;
		savedModel.on('invalid', function (model, error) {
			defer.resolve({ error: error });
		});
		if (savedModel.isNew()) {
	    	var reactions = new Entities.ReactionsCollection();
	    	savedModel.set(attrs);
	    	reactions.create(savedModel, {
	           success : function (data) {
	               defer.resolve({response:data});
	           } 
	        });
	    }
	    else {
			savedModel.save(attrs, {
	            success: function(data) {
	                defer.resolve({response:data});
	            }
	        });
	    }	
	    return defer.promise();
	    
	});
	DrugReactionsManager.reqres.setHandler("reaction:new", function () {
	    return new Entities.Reaction();
    });
	DrugReactionsManager.reqres.setHandler("reaction:entities", function() {
		var reactions = new Entities.ReactionsCollection();
		var defer = $.Deferred();
		reactions.fetch({
			success: function(data, response, options) {
				defer.resolve(data);
			}
		});
		return defer.promise();
	});
	
	DrugReactionsManager.reqres.setHandler("reaction:entity", function (id) {
		var reaction = new Entities.Reaction({id:id});
		var defer = $.Deferred();
		reaction.fetch({
			success: function (data, response, options) {
				defer.resolve(data);
			}
		});
		return defer.promise();
	});

});