﻿var DrugReactionsManager = new Marionette.Application();

DrugReactionsManager.addRegions({
	mainRegion: "#main-region"
});

DrugReactionsManager.navigate = function(route, options){
	options || (options = {});
	Backbone.history.navigate(route, options);
};

DrugReactionsManager.getCurrentRoute = function () {
	return Backbone.history.fragment;
};

DrugReactionsManager.on("initialize:after", function () {
	if (Backbone.history) {
		Backbone.history.start();
	}
	if(this.getCurrentRoute() === "") {
		DrugReactionsManager.trigger('reactions:list');
	}	
});
