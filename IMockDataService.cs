﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;

namespace PharmacyProject
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMockDataService" in both code and config file together.
    [ServiceContract]
    public interface IMockDataService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/reactions", ResponseFormat = WebMessageFormat.Json)]
        List<Reaction> GetReactions();

		[OperationContract]
		[WebGet(UriTemplate = "/reactions/{id}", ResponseFormat = WebMessageFormat.Json)]
		Reaction GetReaction(string id);

        [OperationContract]
		[WebInvoke(Method = "PUT", UriTemplate = "reactions/{id}", ResponseFormat = WebMessageFormat.Json)]
	    string SaveReaction(string id, Reaction reaction);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/reactions", ResponseFormat = WebMessageFormat.Json)]
        string AddReaction(Reaction reaction);
    }

    public class Reaction
    {
        public Int64 Id { get; set; }
        public DateTime ReportDate { get; set; }
        public String CauseType { get; set; }
        public String Cause { get; set; }
        public String ReactionType { get; set; }
        public String ReactionDescription { get; set; }
    }
}
